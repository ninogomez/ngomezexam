##**Tools Needed**##

+ Visual Studio 
+ Selenium
+ Protractor
+ Newtonsoft Json

##** Plugins Needed to Install **##

*NOTE: references are not included in this project. To install follow the steps below.*

1. Right-Click on References in the solution explorer and select **Manage NuGet Packages...**
2. In the NuGet Packages window, search and install the following
      * Newtonsoft.Json 10.0.2
      * Protractor.0.10.2
      * Selenium.Firefox.WebDriver.0.16.1
      * Selenium.Support.3.4.0
      * Selenium.WebDriver.3.4.0
3. Verify if all references are installed in the References node. 

##** How to Use **##
*NOTE: using http://localhost:3000 as the default URL. if needed please update the url in class **UnitTest1**.

1. Open MeWeTestExam.sln file. 
2. In the **Test Explorer** window, right click on each TestMethod and select Run or Click on *Run All* to run all tests. 
      * _01_CheckList
      * _02_CreateAndVerify
      * _03_UpdateAndVerify